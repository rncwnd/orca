/*! \file shader.cpp
    \brief Implementation file for the Shader class.

    This file contains the entire implementation of the Shader class
    as defined in shader.h.
*/
#include "shader.h"
#include "object.h"
#include <iostream>
#include <fstream>

static void LoadShader(std::string* shadertext, const std::string& filename);

/*! \brief Shader class constructor.

    When a Shader object is created, it will load the actual GLSL shaders
    from file, compile them, and store all the endpoints of uniforms and other
    variables found within the shader so that they may be updated at a later
    point.

    @param[in] vshader The name of the vertex shader file on disk
    @param[in] fshader The name of the fragment shader file on disk
*/
Shader::Shader(const std::string vshader, const std::string fshader) {
	m_program                  = glCreateProgram();
	m_shaders[VERTEX_SHADER]   = glCreateShader(GL_VERTEX_SHADER);
	m_shaders[FRAGMENT_SHADER] = glCreateShader(GL_FRAGMENT_SHADER);

	std::string vertexShaderText;
	LoadShader(&vertexShaderText, vshader);
	const GLchar* vertexShaderTextCStr = vertexShaderText.c_str();
	std::string fragmentShaderText;
	LoadShader(&fragmentShaderText, fshader);
	const GLchar* fragmentShaderTextCStr = fragmentShaderText.c_str();
	glShaderSource(m_shaders[VERTEX_SHADER], 1, &vertexShaderTextCStr, NULL);
	glShaderSource(m_shaders[FRAGMENT_SHADER], 1, &fragmentShaderTextCStr, NULL);
	glCompileShader(m_shaders[VERTEX_SHADER]);
	glCompileShader(m_shaders[FRAGMENT_SHADER]);

	glAttachShader(m_program, m_shaders[VERTEX_SHADER]);
	glAttachShader(m_program, m_shaders[FRAGMENT_SHADER]);
	glLinkProgram(m_program);
	glValidateProgram(m_program);

	auto ok = checkErrors();
	if (ok) {
		m_uniforms[MODEL_UNIFORM]       = glGetUniformLocation(m_program, "model");
		m_uniforms[VIEW_UNIFORM]        = glGetUniformLocation(m_program, "view");
		m_uniforms[PERSPECTIVE_UNIFORM] = glGetUniformLocation(m_program, "projection");
		m_uniforms[CAMERA_POS]          = glGetUniformLocation(m_program, "viewPos");

		m_uniforms[MAT_COLOUR1] = glGetUniformLocation(m_program, "material.colour1");
		m_uniforms[MAT_COLOUR2] = glGetUniformLocation(m_program, "material.colour2");
		m_uniforms[SPECULAR]    = glGetUniformLocation(m_program, "material.specular");
		m_uniforms[SHINYNESS]   = glGetUniformLocation(m_program, "material.shininess");
		m_uniforms[INTENSITIES] = glGetUniformLocation(m_program, "material.intensity");

		m_uniforms[MAP_DIFFUSE]  = glGetUniformLocation(m_program, "material.diffuse_map");
		m_uniforms[MAP_SPECULAR] = glGetUniformLocation(m_program, "material.specular_map");

		m_uniforms[LIGHTPOS_UNIFORM] = glGetUniformLocation(m_program, "light.position");
		m_uniforms[LIGHT_COLOR]      = glGetUniformLocation(m_program, "light.color");

		m_uniforms[USE_DIFF_SETTING] = glGetUniformLocation(m_program, "useDiffuseMap");
		m_uniforms[USE_SPEC_SETTING] = glGetUniformLocation(m_program, "useSpecularMap");
	}

	m_cam   = nullptr;
	m_light = nullptr;
}

/*! \brief Shader class destructor.

    When the Shader object gets deleted, this function will automatically
    detach and delete all the shaders that were created during the lifetime
    of the engine.
 */
Shader::~Shader() {
	glDetachShader(m_program, m_shaders[VERTEX_SHADER]);
	glDeleteShader(m_shaders[VERTEX_SHADER]);
	glDetachShader(m_program, m_shaders[FRAGMENT_SHADER]);
	glDeleteShader(m_shaders[FRAGMENT_SHADER]);
	glDeleteProgram(m_program);
}

/*! \brief Changes active shader to the current one.

    When called, this function will set the active shader used by
    OpenGL to this one.
*/
void Shader::bind() { glUseProgram(m_program); }

/*! \brief Links the Camera that the Shader should use

    This function tells the Shader which Camera should
    be used to draw objects.

    @param[in] camera The camera to be used
*/
void Shader::addCamera(Camera* camera) { m_cam = camera; }

/*! \brief Links the Light that the Shader should use

    This function tells the Shader which Light should
    be used to draw objects.

    @param[in] light The Light to be used
*/
void Shader::addLight(Light* light) { m_light = light; }

/*! \brief Links Shader with 3D Object.

    When an object is linked via the Shader, it gets
    added to a list of all objects that use the shader.

    All objects that can be drawn with the shader are then later drawn
    when drawAll() is called. The objects are all drawn together before
    a different shader is loaded.

    If an object isn't linked, the shader won't know about it and
    it will simply not be rendered by the engine.

    @param[in] obj A pointer to an Object that needs to be linked
 */
void Shader::link(Object* obj) { m_objectList.push_back(obj); }

void LoadShader(std::string* shadertext, const std::string& filename) {
	std::ifstream file;
	file.open(filename.c_str());

	std::string line;

	if (file.is_open()) {
		while (file.good()) {
			getline(file, line);
			(*shadertext).append(line + "\n");
		}
	} else {
		std::cerr << "Unable to load shader: " << filename << std::endl;
	}
}

bool Shader::checkErrors() {
	GLint success;
	GLchar infoLog[512];

	glGetShaderiv(m_shaders[VERTEX_SHADER], GL_COMPILE_STATUS, &success);
	if (!success) {
		std::cout << "The vertex shader failed to compile\n";
		glGetShaderInfoLog(m_shaders[VERTEX_SHADER], 512, NULL, infoLog);
		std::cout << "ERROR::SHADER::VERTEX::COMPILATION_FAILED\n" << infoLog << std::endl;
		return false;
	}

	glGetShaderiv(m_shaders[FRAGMENT_SHADER], GL_COMPILE_STATUS, &success);
	if (!success) {
		std::cout << "The fragment shader failed to compile\n";
		glGetShaderInfoLog(m_shaders[FRAGMENT_SHADER], 512, NULL, infoLog);
		std::cout << "ERROR::SHADER::FRAGMENT::COMPILATION_FAILED\n" << infoLog << std::endl;
		return false;
	}

	glGetProgramiv(m_program, GL_LINK_STATUS, &success);
	if (!success) {
		std::cout << "The shader program failed to link\n";
		return false;
	}
	return true;

	glGetProgramiv(m_program, GL_VALIDATE_STATUS, &success);
	if (!success) {
		std::cout << "The shader program failed to link\n";
		return false;
	}
	return true;
}
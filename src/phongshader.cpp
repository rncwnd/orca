/*! \file phongshader.cpp
    \brief Implementation file for the PhongShader class.

    This file contains the entire implementation of the PhongShader class
    as defined in phongshader.h.
*/
#include "phongshader.h"
#include "transformation.h"
#include "object.h"

/*! \brief PhongShader class constructor.

    When a shader object is created, it will load the actual GLSL shaders
    from file, compile them, and store all the endpoints of uniforms and other
    variables found within the shader so that they may be updated at a later
   point.
 */
PhongShader::PhongShader() : Shader("./Shaders/phong_vertex.glsl", "./Shaders/phong_fragment.glsl") {}

/*! \brief Draws objects.

    This is where the drawing of all objects is initiated.

    All objects that have been linked through the link() call
    will be rendered by the engine when this function is called.

    It will update all the shader uniforms neccessary for the objects
    to render correctly.
 */
void PhongShader::drawAll() {
    if (m_cam == nullptr || m_light == nullptr)
        return;
	// Update camera and light uniforms
	glUniformMatrix4fv(m_uniforms[VIEW_UNIFORM], 1, GL_FALSE, glm::value_ptr(m_cam->getViewMat()));
	glUniformMatrix4fv(m_uniforms[PERSPECTIVE_UNIFORM], 1, GL_FALSE, glm::value_ptr(m_cam->getProjectionMat()));
	glm::vec3 cpos = m_cam->getPosition();
	glUniform3f(m_uniforms[CAMERA_POS], cpos.x, cpos.y, cpos.z);

	glm::vec3 lightPos   = m_light->getPosition();
	glm::vec3 lightColor = m_light->getColor();
	glUniform3f(m_uniforms[LIGHTPOS_UNIFORM], lightPos[0], lightPos[1], lightPos[2]);
	glUniform3f(m_uniforms[LIGHT_COLOR], lightColor[0], lightColor[1], lightColor[2]);

	for (unsigned int i = 0; i < m_objectList.size(); i++) {
		Transformation* transformObj = m_objectList[i]->getTransform();
		transformObj->transform();
		glUniformMatrix4fv(m_uniforms[MODEL_UNIFORM], 1, GL_FALSE, transformObj->getTransformationMatrix());

		Material* mat = m_objectList[i]->getMaterial();
		glUniform3f(m_uniforms[MAT_COLOUR1], mat->m_colour1.x, mat->m_colour1.y, mat->m_colour1.z);
		glUniform3f(m_uniforms[SPECULAR], mat->m_spec_col.x, mat->m_spec_col.y, mat->m_spec_col.z);
		glUniform1f(m_uniforms[SHINYNESS], mat->m_shiny);
		glUniform3f(m_uniforms[INTENSITIES], mat->m_intensities[0], mat->m_intensities[1], mat->m_intensities[2]);
		glUniform1i(m_uniforms[MAP_DIFFUSE], 0);
		glUniform1i(m_uniforms[MAP_SPECULAR], 1);
		// Settings
		glUniform1i(m_uniforms[USE_DIFF_SETTING], (int)mat->m_use_diffuse_map);
		glUniform1i(m_uniforms[USE_SPEC_SETTING], (int)mat->m_use_specular_map);

		mat->render();
		m_objectList[i]->getMesh()->draw();
	}
}

/*! \brief PhongShader class destructor.

    When the shader object gets deleted, this function will automatically
    detach and delete all the shaders that were created during the lifetime
    of the engine.
 */
PhongShader::~PhongShader() {}
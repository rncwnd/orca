/*! \file imageloader.h
    \brief Header file for the ImageLoader class.

    This file is the header file for the ImageLoader class.
*/
#ifndef IMAGELOADER_H
#define IMAGELOADER_H

#include <iostream>

/*! \brief Provides an image file loader for the engine.

    This class is a light wrapper around stb_image.h. It provides an interface
    that can be used to load images from file. It supports a wide variety of image 
    file formats.
*/
class ImageLoader {
  public:
	ImageLoader(std::string);
	~ImageLoader();

	int m_width; //!< The width of the image
	int m_height; //!< The height of the image
	int m_numchannels; //!< The number of color channels [1-4] that the image contains
	int m_requestchannels; //!< The number of color channels you want to load
	unsigned char* m_imgdata; //!< A pointer to the actual image data

  private:
};

#endif // IMAGELOADER_H

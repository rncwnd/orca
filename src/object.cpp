/*! \file object.cpp
    \brief Implementation file for the Object class.

    This file contains the entire implementation of the Object class
    as defined in object.h.
*/
#include "object.h"
#include "transformation.h"

/*! \brief Object class constructor

    The constructor combines meshes and materials into single
    objects.

    @param[in] material A pointer to a Material object that should be used
    @param[in] mesh A pointer to a Mesh object, which defines the current
   object.
 */
Object::Object(Material* material, Mesh* mesh) {
	m_mesh      = mesh;
	m_material  = material;
	m_transform = new Transformation();
}

/*! \brief Object class destructor
 */
Object::~Object() { delete m_transform; }

/*! \brief Translates object

    This function allows the user to move objects around
    in the scene.

    @param[in] x The object's x coordinate
    @param[in] y The object's y coordinate
    @param[in] z The object's z cooridnate
 */
void Object::setTranslation(float x, float y, float z) { m_transform->setPosition(x, y, z); }

/*! \brief Scales objects

    This function allows the user to change the scale of objects
    in the scene.

    @param[in] x The object's scale in the x direction
    @param[in] y The object's scale in the y direction
    @param[in] z The object's scale in the z direction
 */
void Object::setScale(float x, float y, float z) { m_transform->setScale(x, y, z); }

/*! \brief Rotates object

    This function allows the user to rotate objects in the scene.

    @param[in] x The amount of rotation in radians
    @param[in] axis One of three possible Axes
 */
void Object::setRotation(float x, Axes axis) { m_transform->setRotation(x, axis); }

/*! \brief Returns object's current position

    This function returns the current position of x coordinatethe object.

    @return A position vector
 */
glm::vec3 Object::getPosition() { return m_transform->getPosition(); }

/*! \brief Returns the current scaling of the object

    This function returns the scaling multipliers of the
    current object.

    @return A scaling vector
 */
glm::vec3 Object::getScale() { return m_transform->getScale(); }

/*! \brief Returns the current rotation of the object

    This function returns the rotation of the current object in radians.

    @param[in] axis The axis of rotation
    @return The rotation in radians
 */
float Object::getRotation(Axes axis) { return m_transform->getRotation(axis); }

/*! \brief Returns the Transform object

    This function returns the Transformation object which is associated
    with this Object.

    @return A Transformation object
 */
Transformation* Object::getTransform() { return m_transform; }

/*! \brief Returns the Material

    This function returns the current Material object which is assciated
    with this Object.

    @return A Material object
 */
Material* Object::getMaterial() { return m_material; }

/*! \brief Returns the Mesh

    This function returns the current Mesh object which is associated with
    this Object.

    @return The Mesh object
*/
Mesh* Object::getMesh() { return m_mesh; }
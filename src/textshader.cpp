#include "textshader.h"
#include "text.h"
#include "transformation.h"
#include "mesh.h"
#include <iostream>
#include <fstream>
#include <glm/gtx/transform.hpp>
#include <cmath>

static void LoadShader(std::string* shadertext, const std::string& filename);

TextShader::TextShader() {
	m_program                  = glCreateProgram();
	m_shaders[VERTEX_SHADER]   = glCreateShader(GL_VERTEX_SHADER);
	m_shaders[FRAGMENT_SHADER] = glCreateShader(GL_FRAGMENT_SHADER);

	std::string vertexShaderText;
	LoadShader(&vertexShaderText, "./Shaders/text_vertex.glsl");
	const GLchar* vertexShaderTextCStr = vertexShaderText.c_str();
	std::string fragmentShaderText;
	LoadShader(&fragmentShaderText, "./Shaders/text_fragment.glsl");
	const GLchar* fragmentShaderTextCStr = fragmentShaderText.c_str();
	glShaderSource(m_shaders[VERTEX_SHADER], 1, &vertexShaderTextCStr, NULL);
	glShaderSource(m_shaders[FRAGMENT_SHADER], 1, &fragmentShaderTextCStr, NULL);
	glCompileShader(m_shaders[VERTEX_SHADER]);
	glCompileShader(m_shaders[FRAGMENT_SHADER]);

	glAttachShader(m_program, m_shaders[VERTEX_SHADER]);
	glAttachShader(m_program, m_shaders[FRAGMENT_SHADER]);
	glLinkProgram(m_program);
	glValidateProgram(m_program);

	auto ok = checkErrors();
	if (ok) {
		m_uniforms[MODEL_UNIFORM] = glGetUniformLocation(m_program, "model");
		m_uniforms[PERSPECTIVE_UNIFORM] = glGetUniformLocation(m_program, "projection");
		m_uniforms[TEX_EXTRA] = glGetUniformLocation(m_program, "texExtra");
		m_uniforms[MAT_COLOUR1] = glGetUniformLocation(m_program, "material.colour1");
		m_uniforms[MAP_DIFFUSE] = glGetUniformLocation(m_program, "material.diffuse_map");
	}
}

void TextShader::drawAll() {
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glm::mat4 projmat = glm::ortho(0.0f, 800.0f * 1.5f, 0.0f, 480.0f * 1.5f);
	glUniformMatrix4fv(m_uniforms[PERSPECTIVE_UNIFORM], 1, GL_FALSE, glm::value_ptr(projmat));

	for (const auto& txt : m_textList) {
		glUniform1i(m_uniforms[MAP_DIFFUSE], 0);
		glUniform3f(m_uniforms[MAT_COLOUR1], txt->m_colour1.x, txt->m_colour1.y, txt->m_colour1.z);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, txt->m_d_texture);

		int fxpos = txt->m_xpos;
		int fypos = txt->m_ypos;
		for (const auto& ch : txt->m_str) {
			Text::Character tc = txt->m_carr[(int)ch];
			if (ch == ' ') {
				fxpos += tc.Advance;
				continue;
			}
			fxpos += tc.Bearing.x + tc.Size.x / 2;

			// std::cout << ch << ", " << (int)ch << ", " << tc.Size.x << ", " << tc.Size.y << ", " << tc.Advance <<
			// std::endl;
			txt->m_trans->setScale(tc.Size.x / 2.0f, tc.Size.y / 2.0f, 1.0f);
			txt->m_trans->setPosition(fxpos, fypos + tc.Pos.y / 2.0f - tc.Bearing.y / 2.0f, 0.0f);
			txt->m_trans->transform();

			glUniformMatrix4fv(m_uniforms[MODEL_UNIFORM], 1, GL_FALSE, txt->m_trans->getTransformationMatrix());
			// std::cout << 1024*((float)tc.Pos.x)/1024.0f << std::endl;
			// std::cout << 32*((float)tc.Pos.y)/32.0f << std::endl;
			// std::cout << 1024*((float)tc.Size.x)/1024.0f << std::endl;
			// std::cout << 32*((float)tc.Size.y)/32.0f << std::endl;
			glUniform4f(m_uniforms[TEX_EXTRA], ((float)tc.Pos.x) / ((float)txt->m_tex_width), ((float)tc.Pos.y) / ((float)txt->m_tex_height),
			            ((float)tc.Size.x) / ((float)txt->m_tex_width), ((float)tc.Size.y) / ((float)txt->m_tex_height));
			fxpos += (float)tc.Size.x / 2;
			txt->m_mesh->draw();
		}
	}

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, 0);
	glDisable(GL_BLEND);
}

void TextShader::bind() { glUseProgram(m_program); }

void TextShader::link(Text* txt) { m_textList.push_back(txt); }

TextShader::~TextShader() {
	glDetachShader(m_program, m_shaders[VERTEX_SHADER]);
	glDeleteShader(m_shaders[VERTEX_SHADER]);
	glDetachShader(m_program, m_shaders[FRAGMENT_SHADER]);
	glDeleteShader(m_shaders[FRAGMENT_SHADER]);
	glDeleteProgram(m_program);
}

void LoadShader(std::string* shadertext, const std::string& filename) {
	std::ifstream file;
	file.open(filename.c_str());

	std::string line;

	if (file.is_open()) {
		while (file.good()) {
			getline(file, line);
			(*shadertext).append(line + "\n");
		}
	} else {
		std::cerr << "Unable to load shader: " << filename << std::endl;
	}
}

bool TextShader::checkErrors() {
	GLint success;
	GLchar infoLog[512];

	glGetShaderiv(m_shaders[VERTEX_SHADER], GL_COMPILE_STATUS, &success);
	if (!success) {
		std::cout << "The vertex shader failed to compile\n";
		glGetShaderInfoLog(m_shaders[VERTEX_SHADER], 512, NULL, infoLog);
		std::cout << "ERROR::SHADER::VERTEX::COMPILATION_FAILED\n" << infoLog << std::endl;
		return false;
	}

	glGetShaderiv(m_shaders[FRAGMENT_SHADER], GL_COMPILE_STATUS, &success);
	if (!success) {
		std::cout << "The fragment shader failed to compile\n";
		glGetShaderInfoLog(m_shaders[FRAGMENT_SHADER], 512, NULL, infoLog);
		std::cout << "ERROR::SHADER::FRAGMENT::COMPILATION_FAILED\n" << infoLog << std::endl;
		return false;
	}

	glGetProgramiv(m_program, GL_LINK_STATUS, &success);
	if (!success) {
		std::cout << "The shader program failed to link\n";
		return false;
	}
	return true;

	glGetProgramiv(m_program, GL_VALIDATE_STATUS, &success);
	if (!success) {
		std::cout << "The shader program failed to link\n";
		return false;
	}
	return true;
}
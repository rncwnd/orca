/*! \file light.cpp
    \brief Implementation file for the Light class.

    This file contains the entire implementation of the Light class
    as defined in light.h.
*/
#include "light.h"

/*! \brief Light class constructor.

    Creates a light with default values
 */
Light::Light(enum lightType ltype) {
	m_position    = glm::vec3(0.0, 0.0, 0.0);
    m_color       = glm::vec3(1.0f, 1.0f, 1.0f);
    m_light_type = ltype;
}

/*! \brief Light class destructor.

 */
Light::~Light() {}

/*! \brief Sets the colour of the light

    Sets the individual intensities of the red, green and blue channels
    of the light colour

    @param[in] r The red channel value
    @param[in] g The green channel value
    @param[in] b The blue channel value
 */
void Light::setColor(GLfloat r, GLfloat g, GLfloat b) {
	m_color.x = r;
	m_color.y = g;
	m_color.z = b;
}

/*! \brief Sets the position of the light

    Sets the position of the light in world space

    @param[in] x The x-axis position
    @param[in] y The y-axis position
    @param[in] z The z-axis position
 */
void Light::setPosition(GLfloat x, GLfloat y, GLfloat z) {
	m_position.x = x;
	m_position.y = y;
	m_position.z = z;
}

/*! \brief Returns the colour of the Light

    Returns a reference to the rgb colour vector of the Light

    @return The Light colour
*/
glm::vec3& Light::getColor(){
    return m_color;
}

/*! \brief Returns a reference to the Light position vector

    Returns a reference to the position vector of the Light

    @return The position vector
*/
glm::vec3& Light::getPosition(){
    return m_position;
}
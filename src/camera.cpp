/*! \file camera.cpp
    \brief Implementation file for the Camera class.

    This file contains the entire implementation of the Camera class
    as defined in camera.h.
*/
#include "camera.h"
#include <iostream>
#include <glm/gtx/transform.hpp>

/*! \brief Camera class constructor.

    @param[in] w The width of the drawing surface
    @param[in] h The height of the drawing surface
    @param[in] type The CAMERA_TYPE (FPS or PERSPECTIVE) to create
 */
Camera::Camera(float w, float h, CAMERA_TYPE type) {
	m_pos        = glm::vec3(0.0f, -1.0f, 0.0f);
	m_focus      = glm::vec3(0.0f);
	m_front      = glm::normalize(m_focus - m_pos);
	m_up         = glm::vec3(0, 0, 1);
	m_view       = glm::lookAt(m_pos, m_focus, m_up);
	m_projection = glm::perspective(3.1415f / 4.0f, w / h, 0.1f, 100.0f);
	m_cam_type   = type;
}

/*! \brief Camera class destructor
*/
Camera::~Camera() {}

/*! \brief Updates the camera information of the shader

    This function will update the view matrix needed by the shader
    for drawing
*/
void Camera::updateViewMatrix() {
	m_right = glm::normalize(glm::cross(m_front, glm::vec3(0, 0, 1)));
	m_up    = glm::normalize(glm::cross(m_right, m_front));
	m_view  = glm::lookAt(m_pos, m_focus, m_up);
}

/*! \brief Moves the camera

    Moves the camera to a new location

    @param[in] x The new x-coordiante
    @param[in] y The new y-coordiante
    @param[in] z the new z-coordiante
*/
void Camera::updatePosition(float x, float y, float z) {
	m_pos.x = x;
	m_pos.y = y;
	m_pos.z = z;
	m_front = glm::normalize(m_focus - m_pos);
}

/*! \brief Moves the camera's focus point

    Moves the camera's focus point to a new location

    @param[in] x The new x-coordiante
    @param[in] y The new y-coordiante
    @param[in] z the new z-coordiante
*/
void Camera::updateFocusPoint(float x, float y, float z) {
	m_focus.x = x;
	m_focus.y = y;
	m_focus.z = z;
	m_front   = glm::normalize(m_focus - m_pos);
}

/*! \brief Updates the camera information of the shader

    This function will update the matricies needed by the shader
    for drawing
*/
void Camera::update() { updateViewMatrix(); }

/*! \brief Moves the camera in one of 4 directions

    Moves the camera in one of 4 directions

    @param[in] dir One of 4 possible DIRECTIONs
    @param[in] amount The distance to move in a certain direction
*/
void Camera::updatePosition(DIRECTION dir, float amount) {
	switch (dir) {
	case FORWARD:
		m_pos += m_front * amount;
		break;
	case BACKWARD:
		m_pos -= m_front * amount;
		break;
	case LEFTWARD:
		m_pos -= glm::normalize(glm::cross(m_front, m_up)) * amount;
		break;
	case RIGHTWARD:
		m_pos += glm::normalize(glm::cross(m_front, m_up)) * amount;
		break;
	default:
		break;
	}
}

/*! \brief Updates the front of the camera

    For FPS cameras, updates where the camera is
    looking

    @param[in] pitch The pitch value
    @param[in] yaw The yaw value
*/
void Camera::updateFront(float pitch, float yaw) {
	glm::vec3 front;
	front.x = sin(glm::radians(yaw)) * cos(glm::radians(pitch));
	front.y = cos(glm::radians(yaw)) * cos(glm::radians(pitch));
	front.z = sin(glm::radians(pitch));
	m_front = glm::normalize(front);
}

/*! \brief Returns a reference to the view matrix

    Returns a reference to the view matrix of the Camera

    @return The view matrix
*/
glm::mat4& Camera::getViewMat() { return m_view; }

/*! \brief Returns a reference to the projection matrix

    Returns a reference to the projection matrix of the Camera

    @return The projection matrix
*/
glm::mat4& Camera::getProjectionMat() { return m_projection; }

/*! \brief Returns a reference to the Camera position vector

    Returns a reference to the position vector of the Camera

    @return The position vector
*/
glm::vec3& Camera::getPosition() { return m_pos; }
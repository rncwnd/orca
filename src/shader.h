/*! \file shader.h
    \brief Header file for the Shader class.

    This file is the header file for the Shader class.
*/
#ifndef SHADER_H
#define SHADER_H

#include <vector>
#include <string>
#include "camera.h"
#include "light.h"

class Object;

/*! \brief Serves as the base class of GLSL shader classes.

    This class serves as the base class from which all other
    shader classes are derived. It stores information about all the
    endpoints of the GLSL shader that provide a way for the engine to
    interface correctly with it.

    It also compiles the shaders, defines the basic capabilities of the
    shader, and implements error checking for GLSL code.
*/
class Shader {
  public:
	Shader(const std::string, const std::string);
	virtual ~Shader();
	void bind();
	void link(Object*);
	void addCamera(Camera*);
	void addLight(Light*);
	virtual void drawAll() = 0;

  protected:
	bool checkErrors();
	enum { VERTEX_SHADER, FRAGMENT_SHADER, NUM_SHADERS };

	enum {
		MODEL_UNIFORM,
		VIEW_UNIFORM,
		PERSPECTIVE_UNIFORM,
		LIGHTPOS_UNIFORM,
		INTENSITIES,
		LIGHT_COLOR,
		CAMERA_POS,
		MAT_COLOUR1,
		MAT_COLOUR2,
		SPECULAR,
		SHINYNESS,
		USE_DIFF_SETTING,
		USE_SPEC_SETTING,
		MAP_DIFFUSE,
		MAP_SPECULAR,
		NUM_UNIFORMS
	};

	GLuint m_program;
	GLuint m_shaders[NUM_SHADERS];
	GLuint m_uniforms[NUM_UNIFORMS];

	std::vector<Object*> m_objectList;
	Camera* m_cam;
	Light* m_light;
};

#endif // SHADER_H
#ifndef TEXT_H
#define TEXT_H

#include "string"
#include <glm/glm.hpp>
#include <glad/glad.h>

class Mesh;
class Transformation;

class Text {
  public:
	struct Character {
		glm::ivec2 Size;    // Size of glyph
		glm::ivec2 Pos;     // Position of glyph
		glm::ivec2 Bearing; // Offset from baseline to left/top of glyph
		GLuint Advance;     // Offset to advance to next glyph
	};

	Text(std::string, int, int);
	~Text();

	glm::vec3 m_colour1; //!< The base colour of the material
	GLuint m_d_texture;
	Transformation* m_trans;
	Mesh* m_mesh;
	Character m_carr[128];
	std::string m_str;
	int m_xpos;
	int m_ypos;
    int m_tex_width;
    int m_tex_height;
  private:
	void loadFont(Character*);
};

#endif // TEXT_H

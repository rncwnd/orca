/*! \file camera.h
    \brief Header file for the Camera class.

    This file is the header file for the Camera class.
*/
#ifndef CAMERA_H
#define CAMERA_H

#include <glm/glm.hpp>

/*! \brief Provides cameras for the engine.

    This class provides both FPS and Perspective camera implementations
    for use in the engine.
*/
class Camera {
  public:
	/*! \brief An enum of possible camera types
	*/
	enum CAMERA_TYPE { PERSPECTIVE, FPS };

	/*! \brief An enum of possible directions
	*/
	enum DIRECTION { FORWARD, BACKWARD, LEFTWARD, RIGHTWARD };

	Camera(float, float, CAMERA_TYPE);
	~Camera();
	void update();
	void updatePosition(float, float, float);
	void updateFocusPoint(float, float, float);
	void updatePosition(DIRECTION, float);
	void updateFront(float, float);
	glm::mat4& getViewMat();
	glm::mat4& getProjectionMat();
	glm::vec3& getPosition();
	CAMERA_TYPE m_cam_type; //!< The type of camera, either Perspective or FPS

  private:
	void updateViewMatrix();

	glm::vec3 m_pos;
	glm::vec3 m_focus;
	glm::vec3 m_front;
	glm::vec3 m_up;
	glm::vec3 m_right;
	glm::mat4 m_view;
	glm::mat4 m_projection;
};

#endif // CAMERA_H
/*! \file orcaEngine.cpp
    \brief Implementation file for the orcaEngine class.

    This file contains the entire implementation of the orcaEngine class
    as defined in orcaEngine.h.
*/
#define GLFW_INCLUDE_GLCOREARB
#include "glad/glad.h"
#include <GLFW/glfw3.h>
#include "orcaEngine.h"
#include "phongshader.h"
#include "gouraudshader.h"
#include "uishader.h"
#include "textshader.h"
#include <iostream>

static int keys[1024] = {0};
static int _scancode  = 0;
static int _mods      = 0;
static GLfloat lastX = 400, lastY = 240;
static bool firstMouse = true;
static GLfloat yaw = 0, pitch = 0;
static bool camerachange = false;
static orcaEngine* activeEngine;

static void keyCallbackFunc(GLFWwindow* w, int key, int scancode, int action, int mods) {
	if (w) {
		_scancode = scancode;
		_mods     = mods;
		if (action == GLFW_PRESS)
			keys[key] = true;
		else if (action == GLFW_RELEASE)
			keys[key] = false;

		if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
			activeEngine->exitEngine();
	}
}

static void mousecallbackfunc(GLFWwindow* w, double key_xpos, double key_ypos) {
	if (w) {
		if (firstMouse) {
			lastX      = key_xpos;
			lastY      = key_ypos;
			firstMouse = false;
		}

		GLfloat xoffset = key_xpos - lastX;
		GLfloat yoffset = lastY - key_ypos;
		lastX           = key_xpos;
		lastY           = key_ypos;

		GLfloat sensitivity = 0.2;
		xoffset *= sensitivity;
		yoffset *= sensitivity;

		yaw += xoffset;
		pitch += yoffset;

		if (pitch > 89.0f)
			pitch = 89.0f;
		if (pitch < -89.0f)
			pitch = -89.0f;

		camerachange = true;
	}
}

/*! \brief orcaEngine class constructor

    This function will initialize the game engine. This includes setting up
    the window manager and all other classes that are neccessary for rendering.

    @param[in] width The width of the display area in pixels
    @param[in] height The height of the display area in pixels
    @param[in] title The text that should be displayed in the window
*/
orcaEngine::orcaEngine(int width, int height, char* title) {
	/* Initialize the library */
	if (!glfwInit())
		return;

	/* Specifiy OpenGL version & profile */
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	/* Create a windowed mode m_window and its OpenGL context */
	m_window = glfwCreateWindow(width, height, title, NULL, NULL);
	if (!m_window) {
		glfwTerminate();
		return;
	}

	/* Load extensions */
	glfwMakeContextCurrent(m_window);
	gladLoadGLLoader((GLADloadproc)glfwGetProcAddress);

	/* Make the m_window's context current */
	glfwMakeContextCurrent(m_window);

	glfwSwapInterval(1);

	glEnable(GL_DEPTH_TEST);

	glClearColor(0.0f, 0.15f, 0.13f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	m_camera = NULL;
	// Create a new shader (<Subject to change>)
	m_phongShader   = new PhongShader();
	m_gouraudShader = new GouraudShader();
	m_uiShader      = new UIShader();
	m_textShader    = new TextShader();

	activeEngine = this;
	glfwSetKeyCallback(m_window, keyCallbackFunc);
}

/*! \brief orcaEngine class destructor
*/
orcaEngine::~orcaEngine() {
	for (unsigned int i = 0; i < m_objects.size(); i++)
		delete m_objects[i];

	delete m_phongShader;
	delete m_gouraudShader;
	delete m_uiShader;
	delete m_textShader;

	glfwTerminate();
}

/*! \brief Determines if the game engine should exit.

    This function returns a boolean value that indicates
    whether or not the game engine is running.

    @return True if game engine is running and false otherwise
*/
bool orcaEngine::isWindowActive() { return !glfwWindowShouldClose(m_window); }

/*! \brief The engine's main render loop

    This function should be called every time a frame
    should be drawn by the engine. It processes user input and
    issues openGL drawing calls.
*/
void orcaEngine::processinput() {
	if (m_camera != nullptr) {
		GLfloat currentFrame = glfwGetTime();
		m_deltaTime          = currentFrame - m_lastFrame;
		m_lastFrame          = currentFrame;

		if (m_camera->m_cam_type == Camera::CAMERA_TYPE::FPS) {
			move_camera();
			rotate_camera();
		}
		m_camera->update();
		m_phongShader->bind();
		m_phongShader->drawAll();
		m_uiShader->bind();
		m_uiShader->drawAll();
		m_textShader->bind();
		m_textShader->drawAll();
		m_gouraudShader->bind();
		m_gouraudShader->drawAll();
	} else {
		std::cout << "The scene is missing a camera object." << std::endl;
	}

	glfwPollEvents();
	glfwSwapBuffers(m_window);
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void orcaEngine::addText(Text* txt) { m_textShader->link(txt); }

/*! \brief Allows a user to set a function which will receive IO events.

    This function accepts another function as a parameter to which IO events
   will
    be sent so that a user may process them.

    @param[in] errorCallback The name of a function that accepts and int and
   const char*
    and returns void.
*/
void orcaEngine::setErrorCallback(void (*errorCallback)(int, const char*)) { glfwSetErrorCallback(errorCallback); }

/*! \brief Exits game engine

    Calling this function will indicate to the game engine that the user
    intends to exit the program and that it should cease all operations.
*/
void orcaEngine::exitEngine() { glfwSetWindowShouldClose(m_window, true); }

/*! \brief Adds a Camera to the game engine

    Adds the specified Camera to the game engine.

    @param[in] cam The Camera to add.
*/
void orcaEngine::attachCamera(Camera* cam) {
	if (cam->m_cam_type == Camera::CAMERA_TYPE::FPS) {
		glfwSetInputMode(m_window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
		glfwSetCursorPosCallback(m_window, mousecallbackfunc);
	} else {
		glfwSetInputMode(m_window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
		glfwSetCursorPosCallback(m_window, NULL);
	}
	m_camera = cam;
	m_phongShader->addCamera(m_camera);
	m_gouraudShader->addCamera(m_camera);
	m_uiShader->addCamera(m_camera);
}

/*! \brief Adds a Light to the game engine

    Adds the specified Light to the game engine.
    @param[in] light The Light to add
*/
void orcaEngine::attachLight(Light* light) {
	m_light = light;
	m_phongShader->addLight(m_light);
	m_gouraudShader->addLight(m_light);
}

/*! \brief Returns a pointer to a newly created Object

    This function will create an Object for the user from
    Mesh and Material objects and then return a pointer to that object.

    @param[in] mat The Material object
    @param[in] mesh The Mesh object
    @return The Object pointer
*/
Object* orcaEngine::createObject(Material* mat, Mesh* mesh) {
	Object* obj = new Object(mat, mesh);
	m_objects.push_back(obj);
	if (mesh->m_is_ui)
		m_uiShader->link(obj);
	else
		m_phongShader->link(obj);
	return obj;
}

void orcaEngine::move_camera() {
	GLfloat cameraSpeed = 2.0f * m_deltaTime;
	if (keys[GLFW_KEY_W])
		m_camera->updatePosition(Camera::FORWARD, cameraSpeed);
	if (keys[GLFW_KEY_S])
		m_camera->updatePosition(Camera::BACKWARD, cameraSpeed);
	if (keys[GLFW_KEY_A])
		m_camera->updatePosition(Camera::LEFTWARD, cameraSpeed);
	if (keys[GLFW_KEY_D])
		m_camera->updatePosition(Camera::RIGHTWARD, cameraSpeed);
}

void orcaEngine::rotate_camera() {
	if (camerachange) {
		m_camera->updateFront(pitch, yaw);
		camerachange = false;
	}
}
/*! \file material.h
    \brief Header file for the Material class.

    This file is the header file for the Material class.
*/
#ifndef MATERIAL_H
#define MATERIAL_H

#include <glad/glad.h>
#include <glm/glm.hpp>

/*! \brief Provides materials for objects in the engine.

    This class provides a means for objects to alter their appearance through
    textures and other means.
*/
class Material {
  public:
	Material();
	Material(const char*);
	Material(const char*, const char*);
	~Material();
	void render();
	glm::vec3 m_colour1; //!< The base colour of the material
	glm::vec3 m_colour2;
	glm::vec3 m_spec_col; //!< The colour of specular hightlights
	GLfloat m_shiny;      //!< The shininess of the material
	bool m_use_diffuse_map;
	bool m_use_specular_map;
    glm::vec3 m_intensities;
  private:
	GLuint m_d_texture;
	GLuint m_s_texture;
};

#endif // MATERIAL_H

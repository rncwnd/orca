/*! \file orcaEngine.h
    \brief Header file for the orcaEngine class.

    This file is the header file for the orcaEngine class.
*/
#ifndef ORCAENGINE_H
#define ORCAENGINE_H

#include "camera.h"
#include "light.h"
#include "object.h"
#include "text.h"
#include <vector>

class PhongShader;
class GouraudShader;
class UIShader;
class TextShader;
class Light;

enum Axes : unsigned short { X_AXIS, Y_AXIS, Z_AXIS };

/*! \brief Implements the engine which interfaces with user code.

    This class serves as the main access point to the game engine. It
    arranges for the drawing of objects as well as the managegement of
    input from the keyboard and mouse.
*/
class orcaEngine {
  public:
	orcaEngine(int width, int height, char* title = (char*)"Orca");
	~orcaEngine();
	void setErrorCallback(void (*errorCallback)(int, const char*));
	void attachCamera(Camera*);
	void attachLight(Light*);
	void addText(Text*);
	Object* createObject(Material*, Mesh*);
	bool isWindowActive();
	void exitEngine();
	void processinput();

  private:
	GLFWwindow* m_window;
	std::vector<Object*> m_objects;
	Camera* m_camera;
	PhongShader* m_phongShader;
	GouraudShader* m_gouraudShader;
	UIShader* m_uiShader;
	TextShader* m_textShader;

	Light* m_light;
	GLfloat m_deltaTime = 0.0f;
	GLfloat m_lastFrame = 0.0f;
	void move_camera();
	void rotate_camera();
};

#endif // ORCAENGINE_H

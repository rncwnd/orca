/*! \file object.h
    \brief Header file for the Object class.

    This file is the header file for the Object class.
*/
#ifndef OBJECT_H
#define OBJECT_H

#include "material.h"
#include "mesh.h"

class Transformation;
enum Axes : unsigned short;

/*! \brief Serves as a container for 3D objects.

    This class groups together other classes that each implement
    a specific aspect of a 3D object.

    It serves as a single access point for the Mesh, Material and
    Transformation classes that make up a 3D object.

    It also provides a means of changing the physical location and
    dimensions of the object.
*/
class Object {
  public:
	Object(Material*, Mesh*);
	~Object();

	void setTranslation(float, float, float);
	void setScale(float, float, float);
	void setRotation(float, Axes);
	glm::vec3 getPosition();
	glm::vec3 getScale();
	float getRotation(Axes);
	Transformation* getTransform();
	Material* getMaterial();
	Mesh* getMesh();

  private:
	Mesh* m_mesh;
	Material* m_material;
	Transformation* m_transform;
};

#endif // OBJECT_H
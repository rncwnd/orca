/*! \file mesh.cpp
    \brief Implementation file for the Mesh class.

    This file contains the entire implementation of the Mesh class
    as defined in mesh.h.
*/
#include "mesh.h"
#include <fstream>
#include <string>
#include <string.h>
#include <iostream>
#include <vector>
// #include <stdio.h>

void loadOBJ(const char* filename, GLfloat*& v_arr, GLfloat*& t_arr, GLfloat*& n_arr, int* numverts) {
	std::ifstream file(filename);
	std::string str;
	std::vector<GLfloat> vertArray;
	std::vector<GLfloat> texArray;
	std::vector<GLfloat> normalArray;
	std::vector<int> indexArray;

	while (std::getline(file, str)) {
		if (str[0] == '#')
			continue;

		const char* temp    = str.c_str();
		const char delim[2] = " ";
		char* token;

		/* get the first token */
		token = strtok((char*)temp, delim);
		if (token != NULL && strcmp(token, "v") == 0) {
			token = strtok(NULL, delim);
			/* walk through other tokens */
			while (token != NULL) {
				vertArray.push_back(atof(token));
				token = strtok(NULL, delim);
			}
		}
		if (token != NULL && strcmp(token, "vt") == 0) {
			token = strtok(NULL, delim);
			/* walk through other tokens */
			int numt = 0;
			while (token != NULL) {
				if (numt < 2)
					texArray.push_back(atof(token));
				token = strtok(NULL, delim);
				numt++;
			}
		}
		if (token != NULL && strcmp(token, "vn") == 0) {
			token = strtok(NULL, delim);
			/* walk through other tokens */
			while (token != NULL) {
				normalArray.push_back(atof(token));
				token = strtok(NULL, delim);
			}
		}
		if (token != NULL && strcmp(token, "f") == 0) {
			token = strtok(NULL, delim);
			/* walk through other tokens */
			while (token != NULL) {
				int pos1 = 0;
				int pos2 = 0;
				int pos3 = strlen(token) - 1;
				while (token[pos1] != '/')
					pos1++;
				pos2 = pos1 + 1;
				while (token[pos2] != '/')
					pos2++;

				int a = 0, b = 0, c = 0;
				char* vs = new char[pos1 + 1];
				char* fs = new char[pos2 - pos1];
				char* ns = new char[pos3 - pos2 + 1];

				strncpy(vs, token, pos1);
				vs[pos1] = '\0';
				strncpy(fs, token + pos1 + 1, pos2 - pos1 - 1);
				fs[pos2 - pos1 - 1] = '\0';
				strncpy(ns, token + pos2 + 1, pos3 - pos2);
				ns[pos3 - pos2] = '\0';
				a               = atoi(vs);
				b               = atoi(fs);
				c               = atoi(ns);

				indexArray.push_back(a);
				indexArray.push_back(b);
				indexArray.push_back(c);
				token = strtok(NULL, delim);
				delete[] vs;
				delete[] fs;
				delete[] ns;
			}
		}
	}

	v_arr      = new GLfloat[3 * (int)(indexArray.size() / 3)];
	t_arr      = new GLfloat[2 * (int)(indexArray.size() / 3)];
	n_arr      = new GLfloat[3 * (int)(indexArray.size() / 3)];
	int tcount = 0;
	for (int i = 0; i < (int)indexArray.size(); i += 3) {
		v_arr[i + 0] = vertArray[3 * (indexArray[i] - 1) + 0];
		v_arr[i + 1] = vertArray[3 * (indexArray[i] - 1) + 1];
		v_arr[i + 2] = vertArray[3 * (indexArray[i] - 1) + 2];

		t_arr[tcount + 0] = texArray[2 * (indexArray[i + 1] - 1) + 0];
		t_arr[tcount + 1] = texArray[2 * (indexArray[i + 1] - 1) + 1];

		n_arr[i + 0] = normalArray[3 * (indexArray[i + 2] - 1) + 0];
		n_arr[i + 1] = normalArray[3 * (indexArray[i + 2] - 1) + 1];
		n_arr[i + 2] = normalArray[3 * (indexArray[i + 2] - 1) + 2];

		tcount += 2;
	}
	*numverts = (int)indexArray.size() / 3;
	return;
}

Mesh::Mesh() {
	GLfloat m_array[] = {-1.0f, -1.0f, 0.0f, -1.0f, 1.0f,  0.0f, 1.0f,  1.0f,  0.0f,
	                     1.0f,  1.0f,  0.0f, 1.0f,  -1.0f, 0.0f, -1.0f, -1.0f, 0.0f};
	GLfloat texCoords[] = {0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f};
	m_numVerts          = 6;

	glGenVertexArrays(1, &m_vertexArrayObject);
	glBindVertexArray(m_vertexArrayObject);

	glGenBuffers(NUM_BUFFERS, m_vertexBufferObject);

	glBindBuffer(GL_ARRAY_BUFFER, m_vertexBufferObject[POS_BUFFER]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(m_array[0]) * m_numVerts * 3, m_array, GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), 0);
	glEnableVertexAttribArray(0);

	glBindBuffer(GL_ARRAY_BUFFER, m_vertexBufferObject[TEX_BUFFER]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(texCoords[0]) * m_numVerts * 2, texCoords, GL_STATIC_DRAW);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(GLfloat), 0);
	glEnableVertexAttribArray(1);

	glBindVertexArray(0);

	m_is_ui = true;
}

/*! \brief Mesh class constructor

    The constructor will create a Mesh object by reading mesh data
    from a specified OBJ file.

    @param[in] fileName The name of the .obj file
*/
Mesh::Mesh(const char* fileName) {
	GLfloat* m_array    = NULL;
	GLfloat* texCoords  = NULL;
	GLfloat* normCoords = NULL;
	loadOBJ(fileName, m_array, texCoords, normCoords, &m_numVerts);

	glGenVertexArrays(1, &m_vertexArrayObject);
	glBindVertexArray(m_vertexArrayObject);

	glGenBuffers(NUM_BUFFERS, m_vertexBufferObject);
	glBindBuffer(GL_ARRAY_BUFFER, m_vertexBufferObject[POS_BUFFER]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(m_array[0]) * m_numVerts * 3, m_array, GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), 0);
	glEnableVertexAttribArray(0);

	glBindBuffer(GL_ARRAY_BUFFER, m_vertexBufferObject[TEX_BUFFER]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(texCoords[0]) * m_numVerts * 2, texCoords, GL_STATIC_DRAW);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(GLfloat), 0);
	glEnableVertexAttribArray(1);

	glBindBuffer(GL_ARRAY_BUFFER, m_vertexBufferObject[NORM_BUFFER]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(normCoords[0]) * m_numVerts * 3, normCoords, GL_STATIC_DRAW);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), 0);
	glEnableVertexAttribArray(2);

	glBindVertexArray(0);
	delete[] m_array;
	delete[] texCoords;
	delete[] normCoords;

	m_is_ui = false;
}

/*! \brief Mesh class destructor
*/
Mesh::~Mesh() {
	glDeleteBuffers(NUM_BUFFERS, m_vertexBufferObject);
	glDeleteVertexArrays(1, &m_vertexArrayObject);
}

/*! \brief Draws the object

    This function issues the appropirate OpenGL draw calls
    in order to render the object on screen.
*/
void Mesh::draw() {
	glBindVertexArray(m_vertexArrayObject);
	glDrawArrays(GL_TRIANGLES, 0, m_numVerts);
	glBindVertexArray(0);
}
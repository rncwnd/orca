#include "text.h"
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <cstring>
#include "mesh.h"
#include "transformation.h"
#include "imageloader.h"

void Text::loadFont(Character* arr) {
	std::ifstream file("./res/fonts/font4.txt");
	std::string str;
	while (std::getline(file, str)) {
		const char* temp    = str.c_str();
		const char delim[2] = " ";
		char* token;

		int index = -1;
		/* get the first token */
		token = strtok((char*)temp, delim);
		if (token != NULL && strcmp(token, "char") == 0) {
			token         = strtok(NULL, delim);
			token         = strchr(token, '=');
			index         = atoi(++token);
			Character& ch = (*(arr + index));

			token    = strtok(NULL, delim);
			token    = strchr(token, '=');
			ch.Pos.x = atoi(++token);

			token    = strtok(NULL, delim);
			token    = strchr(token, '=');
			ch.Pos.y = atoi(++token);

			token     = strtok(NULL, delim);
			token     = strchr(token, '=');
			ch.Size.x = atoi(++token);

			token     = strtok(NULL, delim);
			token     = strchr(token, '=');
			ch.Size.y = atoi(++token);

			token        = strtok(NULL, delim);
			token        = strchr(token, '=');
			ch.Bearing.x = atoi(++token);

			token        = strtok(NULL, delim);
			token        = strchr(token, '=');
			ch.Bearing.y = atoi(++token);

			token      = strtok(NULL, delim);
			token      = strchr(token, '=');
			ch.Advance = atoi(++token);

			ch.Pos.y = 32 - (ch.Size.y + ch.Pos.y);
		}
	}
}

Text::Text(std::string str, int xpos, int ypos) {
	m_str  = str;
	m_xpos = xpos;
	m_ypos = ypos;

	loadFont(m_carr);

	ImageLoader ldr("./res/fonts/font5.png");
	m_tex_width       = ldr.m_width;
	m_tex_height      = ldr.m_height;
	m_colour1         = glm::vec3(1.0f, 0.0f, 0.0f);
	m_d_texture       = 0;

	glPixelStorei(GL_UNPACK_ALIGNMENT, 1); // Disable byte-alignment restriction

	glGenTextures(1, &m_d_texture);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, m_d_texture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, ldr.m_width, ldr.m_height, 0, GL_RGBA, GL_UNSIGNED_BYTE, ldr.m_imgdata);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glPixelStorei(GL_UNPACK_ALIGNMENT, 4);

	glBindTexture(GL_TEXTURE_2D, 0);

	m_mesh  = new Mesh();
	m_trans = new Transformation();
}

Text::~Text() {
	delete m_mesh;
	delete m_trans;
}
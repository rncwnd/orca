/*! \file transformation.h
    \brief Header file for the Transformation class.

    This file is the header file for the Transformation class.
*/
#ifndef TRANSFORMATION_h
#define TRANSFORMATION_h

#include <glad/glad.h>
#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/quaternion.hpp>

/** @enum Axes
 *  @brief An enum type of possible axes of rotation
 */
enum Axes : unsigned short { X_AXIS, Y_AXIS, Z_AXIS };

/*! \brief Allows objects to be physically transformed in 3D space.

    This class implements interfaces that make the translation, rotation and
    scaling of objects in the scene possible.
*/
class Transformation {
  public:
	Transformation();
	~Transformation();

	void setPosition(float, float, float);
	void setScale(float, float, float);
	void setRotation(float, Axes);
	glm::vec3 getPosition();
	glm::vec3 getScale();
	float getRotation(Axes);
	void transform();

	GLfloat* getTransformationMatrix();

  private:
	glm::vec3 m_pos;
	glm::vec3 m_rot;
	glm::vec3 m_sca;
	glm::mat4 m_transform;
	glm::quat m_qrot;
};

#endif // TRANSFORMATION_h
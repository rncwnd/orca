/*! \file phongshader.h
    \brief Header file for the PhongShader class.

    This file is the header file for the PhongShader class.
*/
#ifndef PHONGSHADER_H
#define PHONGSHADER_H

#include "shader.h"

/*! \brief Serves as an interface to a GLSL phong shader.

    This class stores information about all the endpoints of
    the GLSL shader that provide a way for the engine to interface
    correctly with it.

    It compiles the shader and ultimately defines the capabilities of the
    shader as well as what is required by the engine for successful draw calls.
*/
class PhongShader : public Shader {
  public:
	PhongShader();
	~PhongShader();

	void drawAll();

  private:
};

#endif // PHONGSHADER_H
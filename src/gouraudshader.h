/*! \file gouraudshader.h
    \brief Header file for the GouraudShader class.

    This file is the header file for the GouraudShader class.
*/
#ifndef GOURAUDSHADER_H
#define GOURAUDSHADER_H

#include "shader.h"

/*! \brief Provides a Gouraud shader for the engine.

    This class implements Gouraud shading for the engine.
*/
class GouraudShader : public Shader {
  public:
	GouraudShader();
	~GouraudShader();

	void drawAll();

  private:
};

#endif // GOURAUDSHADER_H

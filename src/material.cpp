/*! \file material.cpp
    \brief Implementation file for the Material class.

    This file contains the entire implementation of the Material class
    as defined in material.h.
*/
#include "material.h"
#include <iostream>
#include "imageloader.h"
/*! \brief Material class constructor.

    Creates a Material object that does not have a diffuse texture.

    The colour of the specular highlights of the material is automatically
    set to white and the shinyness to an appropriate default value.
 */
Material::Material() {
	m_colour1          = glm::vec3(1.0f, 1.0f, 1.0f);
	m_colour2          = glm::vec3(1.0f, 1.0f, 1.0f);
	m_spec_col         = glm::vec3(0.0f, 0.0f, 0.0f);
	m_shiny            = 32.0f;
	m_d_texture        = 0;
	m_use_diffuse_map  = false;
	m_use_specular_map = false;
	m_intensities      = glm::vec3(0.1f, 1.0f, 0.0f);
}

/*! \brief Material class constructor.

    When a Material object is created, the specified diffuse texture is loaded
    from file, and upload to video card memory.

    The colour of the specular highlights of the material is also automatically
    set to white and the shinyness to an appropriate default value.

    @param[in] filename The name of the diffuse texture file
 */
Material::Material(const char* filename) : Material() {
    ImageLoader ldr(filename);

	glGenTextures(1, &m_d_texture);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, m_d_texture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, ldr.m_width, ldr.m_height, 0, GL_RGB, GL_UNSIGNED_BYTE, ldr.m_imgdata);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glGenerateMipmap(GL_TEXTURE_2D);

	glBindTexture(GL_TEXTURE_2D, 0);

	m_use_diffuse_map = true;
}

Material::Material(const char* dfilename, const char* sfilename) : Material(dfilename) {
    ImageLoader ldr(sfilename);

	glGenTextures(1, &m_s_texture);
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, m_s_texture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, ldr.m_width, ldr.m_height, 0, GL_RGB, GL_UNSIGNED_BYTE, ldr.m_imgdata);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glGenerateMipmap(GL_TEXTURE_2D);

	glBindTexture(GL_TEXTURE_2D, 0);

	m_use_specular_map = true;
}

/*! \brief Material class destructor.

    The destructor deletes the texture which is associated with
    the Material object.
 */
Material::~Material() {
	glDeleteTextures(1, &m_d_texture);
	glDeleteTextures(1, &m_s_texture);
}

/*! \brief Activates the diffuse texture

    Calling this function will connect the diffuse texture associated
    with this material, with the diffuse texture slot in the shader.
 */
void Material::render() {
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, m_d_texture);
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, m_s_texture);
}
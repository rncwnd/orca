/*! \file mesh.h
    \brief Header file for the Mesh class.

    This file is the header file for the Mesh class.
*/
#ifndef MESH_H
#define MESH_H

#include <glad/glad.h>
#include <GLFW/glfw3.h>

/*! \brief Stores the vertex information of a 3D mesh.

    This class stores, amongs other things, the vertex, texture and
    normal coordinates of a 3D object. It also implements a function
    to allow the loading of meshes from .obj files.
*/
class Mesh {
  public:
	Mesh();
	Mesh(const char*);
	~Mesh();
	void draw();

	bool m_is_ui;

  private:
	int m_numVerts;
	enum { POS_BUFFER, TEX_BUFFER, NORM_BUFFER, NUM_BUFFERS };

	GLuint m_vertexArrayObject;
	GLuint m_vertexBufferObject[NUM_BUFFERS];
};

#endif // MESH_H

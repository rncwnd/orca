#include "uishader.h"

#include "transformation.h"
#include "object.h"

UIShader::UIShader() : Shader("./Shaders/ui_vertex.glsl", "./Shaders/ui_fragment.glsl") {}

UIShader::~UIShader() {}

void UIShader::drawAll() {
	for (unsigned int i = 0; i < m_objectList.size(); i++) {
		Transformation* transformObj = m_objectList[i]->getTransform();
		transformObj->transform();
		glUniformMatrix4fv(m_uniforms[MODEL_UNIFORM], 1, GL_FALSE, transformObj->getTransformationMatrix());

		Material* mat = m_objectList[i]->getMaterial();
		glUniform3f(m_uniforms[MAT_COLOUR1], mat->m_colour1.x, mat->m_colour1.y, mat->m_colour1.z);
		glUniform3f(m_uniforms[MAT_COLOUR2], mat->m_colour2.x, mat->m_colour2.y, mat->m_colour2.z);
		glUniform1i(m_uniforms[MAP_DIFFUSE], 0);
		glUniform1i(m_uniforms[USE_DIFF_SETTING], (int)mat->m_use_diffuse_map);

		mat->render();
		m_objectList[i]->getMesh()->draw();
	}
}
#ifndef UISHADER_H
#define UISHADER_H

#include "shader.h"

class UIShader : public Shader {
  public:
	UIShader();
	~UIShader();
	void drawAll();

  private:
};

#endif // UISHADER_H

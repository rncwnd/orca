/*! \file light.h
    \brief Header file for the Light class.

    This file is the header file for the Light class.
*/
#ifndef LIGHT_H
#define LIGHT_H

#include <glm/glm.hpp>
#include <glad/glad.h>

/*! \brief Provides lights for the engine.

    This class provides a way to create and manage lights that
    affect objects in the world. A minimum of one light is required
    in order to render 3D Object.
*/
class Light {
  public:
    enum lightType {
        DIRECTIONAL_LIGHT,
        POINT_LIGHT,
        SPOT_LIGHT
    };
    
    Light(enum lightType);
	~Light();

	void setColor(GLfloat, GLfloat, GLfloat);
	void setPosition(GLfloat, GLfloat, GLfloat);
    glm::vec3& getColor();
    glm::vec3& getPosition();

  private:
	glm::vec3 m_position;
    glm::vec3 m_color;
    enum lightType m_light_type;
};

#endif // LIGHT_H

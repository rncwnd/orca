/*! \file gouraudshader.cpp
    \brief Implementation file for the GouraudShader class.

    This file contains the entire implementation of the GouraudShader class
    as defined in gouraudshader.h.
*/
#include "gouraudshader.h"
#include "transformation.h"
#include "object.h"

/*! \brief GouraudShader class constructor
*/
GouraudShader::GouraudShader() : Shader("./Shaders/gouraud_vertex.glsl", "./Shaders/gouraud_fragment.glsl") {}

/*! \brief GouraudShader class destructor
*/
GouraudShader::~GouraudShader() {}

/*! \brief Draws all objects linked to the shader

    This function is where the drawing of all objects that use the 
    GouraudShader are drawn.
*/
void GouraudShader::drawAll() {
    if (m_cam == nullptr || m_light == nullptr)
        return;
	// Update camera and light uniforms
	glUniformMatrix4fv(m_uniforms[VIEW_UNIFORM], 1, GL_FALSE, glm::value_ptr(m_cam->getViewMat()));
	glUniformMatrix4fv(m_uniforms[PERSPECTIVE_UNIFORM], 1, GL_FALSE, glm::value_ptr(m_cam->getProjectionMat()));
	glm::vec3 cpos = m_cam->getPosition();
	glUniform3f(m_uniforms[CAMERA_POS], cpos.x, cpos.y, cpos.z);

	glm::vec3 lightPos   = m_light->getPosition();
	glm::vec3 lightColor = m_light->getColor();
	glUniform3f(m_uniforms[LIGHTPOS_UNIFORM], lightPos[0], lightPos[1], lightPos[2]);
	glUniform3f(m_uniforms[LIGHT_COLOR], lightColor[0], lightColor[1], lightColor[2]);

	for (unsigned int i = 0; i < m_objectList.size(); i++) {
		Transformation* transformObj = m_objectList[i]->getTransform();
		transformObj->transform();
		glUniformMatrix4fv(m_uniforms[MODEL_UNIFORM], 1, GL_FALSE, transformObj->getTransformationMatrix());

		Material* mat = m_objectList[i]->getMaterial();
		glUniform3f(m_uniforms[SPECULAR], mat->m_spec_col.x, mat->m_spec_col.y, mat->m_spec_col.z);
		glUniform1f(m_uniforms[SHINYNESS], mat->m_shiny);
		glUniform3f(m_uniforms[INTENSITIES], mat->m_intensities[0], mat->m_intensities[1], mat->m_intensities[2]);
		mat->render();

		Mesh* mesh = m_objectList[i]->getMesh();
		mesh->draw();
	}
}
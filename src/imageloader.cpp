/*! \file imageloader.cpp
    \brief Implementation file for the ImageLoader class.

    This file contains the entire implementation of the ImageLoader class
    as defined in imageloader.h.
*/
#include "imageloader.h"
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

/*! \brief ImageLoader class constructor.

    @param[in] filename The filename of the image that needs to be laoded.
 */
ImageLoader::ImageLoader(std::string filename) {
	stbi_set_flip_vertically_on_load(true);

	m_requestchannels = 0;
	m_imgdata         = stbi_load(filename.c_str(), &m_width, &m_height, &m_numchannels, m_requestchannels);
	if (m_imgdata == NULL)
		exit(-1);
}

/*! \brief ImageLoader class destructor.
 */
ImageLoader::~ImageLoader() { stbi_image_free(m_imgdata); }
#ifndef TEXTSHADER_H
#define TEXTSHADER_H

#include <vector>
#include <string>
#include <glm/glm.hpp>
#include <glad/glad.h>

class Object;
class Text;

class TextShader {
  public:
	TextShader();
	~TextShader();
	void bind();
	void link(Text*);
	void drawAll();

  private:
	bool checkErrors();
	enum { VERTEX_SHADER, FRAGMENT_SHADER, NUM_SHADERS };
	enum {
		MODEL_UNIFORM,
		PERSPECTIVE_UNIFORM,
		TEX_EXTRA,
		MAT_COLOUR1,
		MAP_DIFFUSE,
		NUM_UNIFORMS
	};

	GLuint m_program;
	GLuint m_shaders[NUM_SHADERS];
	GLuint m_uniforms[NUM_UNIFORMS];

	std::vector<Text*> m_textList;
};

#endif // TEXTSHADER_H

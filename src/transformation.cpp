/*! \file transformation.cpp
    \brief Implementation file for the Transformation class.

    This file contains the entire implementation of the Transformation class
    as defined in transformation.h.
*/
#include "transformation.h"

/*! \brief Transformation class constructor.

    The constructor creates a default model matrix that has zero rotation
    zero translation and unit scaling.
*/
Transformation::Transformation() {
	m_pos = glm::vec3(0.0f);
	m_rot = glm::vec3(0.0f);
	m_sca = glm::vec3(1.0f);

	m_qrot = glm::quat(1.0, 0.0, 0.0, 0.0);
}

/*! \brief Transformation class destructor.

*/
Transformation::~Transformation() {}

/*! \brief Creates model matrix.

    Calling this function will combine the rotation, translation and scaling
    parameters set earlier and create a model matrix that can be used by the
   shaders.

    It is not necessary to call this function as it is automatically called when
   necessary
    by the PhongShader class.
*/
void Transformation::transform() {
	glm::mat4 smat = glm::scale(m_sca);
	glm::mat4 tmat = glm::translate(m_pos);

	glm::vec3 rot = m_rot / 2.0f;
	float c_rot_z = cos(rot.z);
	float c_rot_y = cos(rot.y);
	float c_rot_x = cos(rot.x);
	float s_rot_z = sin(rot.z);
	float s_rot_y = sin(rot.y);
	float s_rot_x = sin(rot.x);

	m_qrot.w = c_rot_z * c_rot_y * c_rot_x + s_rot_z * s_rot_y * s_rot_x;
	m_qrot.x = c_rot_z * c_rot_y * s_rot_x - s_rot_z * s_rot_y * c_rot_x;
	m_qrot.y = c_rot_z * s_rot_y * c_rot_x + s_rot_z * c_rot_y * s_rot_x;
	m_qrot.z = s_rot_z * c_rot_y * c_rot_x - c_rot_z * s_rot_y * s_rot_x;

	glm::mat4 qrmat = glm::mat4_cast(m_qrot);

	m_transform = tmat * qrmat * smat;
}

/*! \brief Sets object position

    Calling this function will update the x, y and z cooridnates of
    the 3D object.

    @param[in] x The x-axis position
    @param[in] y The y-axis position
    @param[in] z The z-axis position
*/
void Transformation::setPosition(float x, float y, float z) { m_pos = glm::vec3(x, y, z); }

/*! \brief Sets object scale

    Calling this function will update the x, y and z scaling
    multiplier.

    @param[in] x The x-axis scaling multiplier
    @param[in] y The y-axis scaling multiplier
    @param[in] z The z-axis scaling mulitplier
*/
void Transformation::setScale(float x, float y, float z) { m_sca = glm::vec3(x, y, z); }

/*! \brief Sets object rotation

    Calling this function will update the euler angles of
    rotation around the x, y and z-axes

    @param[in] angle The angle of rotation in radians
    @param[in] axis One of the 3 possible Axes around which
    the rotation takes place
*/
void Transformation::setRotation(float angle, Axes axis) {
	if (axis == X_AXIS)
		m_rot.x = angle;
	if (axis == Y_AXIS)
		m_rot.y = angle;
	if (axis == Z_AXIS)
		m_rot.z = angle;
}

/*! \brief Gets object position

    Calling this function will return the objects' position
    vector

    @return The position vector
*/
glm::vec3 Transformation::getPosition() { return m_pos; }

/*! \brief Gets object position

    Calling this function will return the objects' position
    vector

    @return The position vector
*/
glm::vec3 Transformation::getScale() { return m_sca; }

/*! \brief Gets object rotation

    Calling this function will return the objects' rotation
    around a specific axis

    @param[in] axis The axis of rotation
    @return The angle of rotation in radians
*/
float Transformation::getRotation(Axes axis) { return m_rot[axis]; }

/*! \brief Gets the model matrix

    Calling this function will return a pointer to the actual model matrix
    of the current Transformation object

    @return A pointer to the model matrix
*/
GLfloat* Transformation::getTransformationMatrix() { return glm::value_ptr(m_transform); }
CC=g++
CFLAGS=-c -Wall -Werror -Wextra -pedantic -std=c++11
LDFLAGS=
SOURCES=$(wildcard ./src/*.cpp)
OBJS=$(addprefix obj/,$(notdir $(SOURCES:.cpp=.o)))
LIBRARY=liborca.a

LIBS=-lglfw3 -lGL -lX11 -lXi -lXrandr -lXxf86vm -lXinerama -lXcursor -pthread -ldl
LIB_DIR=-L/usr/lib/nvidia-375 -L../GLFW/src/ -L/usr/lib/x86_64-linux-gnu
INCLUDE_DIR=-I../GLFW/include/ -I../GLAD/include/ -I../glm/

.PHONY: all clean docs

all: $(LIBRARY)

$(LIBRARY): $(OBJS)
	ar -rcs $@ obj/glad.o $(OBJS)

obj/%.o: src/%.cpp
	$(CC) $(CFLAGS) $(INCLUDE_DIR) -o $@ $<

clean:
	rm $(OBJS) $(LIBRARY)

docs:
	doxygen Documentation/doxyconfig